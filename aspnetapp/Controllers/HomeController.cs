﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BattlestaHealthChecks.Models;
using BattlestaHealthChecks.Interfeces.Services;

namespace BattlestaHealthChecks.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISampleSettingsService _settingsService;
        private readonly ISampleService _sampleService;
        public HomeController(ISampleSettingsService settingsService, ISampleService sampleService)
        {
            _settingsService = settingsService;
            _sampleService = sampleService;
        }
        public async Task<IActionResult> Index(int page = 1)
        {
            var raports = await _sampleService.GetSamples(page);
            return View(raports);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public async Task<IActionResult> ShowDetails(int sampleId)
        {
            if (sampleId == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            var sample = await _sampleService.GetSample(sampleId);

            return View(sample);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> ShowSettings(SampleSettings settings) {
            var model = settings;
            if (model.TimeInterval == 0)
            {
                model = await _settingsService.GetSettings();
            }
            return View(model);
            //return View(new SampleSettings {TimeInterval = 10, MaxLoadTime = 3, StartPage = "http://dev.battlesta.sh/", Email = ""});
        }

        [HttpPost]
        public async Task<IActionResult> SaveSettings(SampleSettings settings)
        { 
            var newSettings = new SampleSettings();
            if(ModelState.IsValid){
                newSettings = await _settingsService.UpdateSettings(settings);
                ViewBag.UpdateSucceeded = "Zaktualizowano ustawienia";
            }
            return RedirectToAction("ShowSettings", "Home", newSettings);
        }
    }
}
