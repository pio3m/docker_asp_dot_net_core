﻿using BattlestaHealthChecks.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BattlestaHealthChecks.Interfeces.Services
{
    public interface ISampleSettingsService
    {
        Task<SampleSettings> GetSettings();
        Task<SampleSettings> UpdateSettings(SampleSettings newSettings);
    }
}
