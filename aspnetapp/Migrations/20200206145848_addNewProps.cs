﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BattlestaHealthChecks.Migrations
{
    public partial class addNewProps : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaxLoadTime",
                table: "SampleSettings",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxLoadTime",
                table: "SampleSettings");
        }
    }
}
