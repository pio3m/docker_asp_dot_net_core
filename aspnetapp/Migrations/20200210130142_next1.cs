﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BattlestaHealthChecks.Migrations
{
    public partial class next1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StartPage",
                table: "SampleSettings",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StartPage",
                table: "SampleSettings");
        }
    }
}
