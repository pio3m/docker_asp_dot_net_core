﻿using BattlestaHealthChecks.Context;
using BattlestaHealthChecks.Interfeces.Services;
using BattlestaHealthChecks.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BattlestaHealthChecks.Services
{
    public class SampleSettingsService : ISampleSettingsService
    {
        private readonly DatabaseContext _context;
        public SampleSettingsService(DatabaseContext context)
        {
            _context = context;
        }
        public async Task<SampleSettings> GetSettings()
        {
            return await _context.SampleSettings.AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<SampleSettings> UpdateSettings(SampleSettings newSettings)
        {
            var settings = await _context.SampleSettings.AsNoTracking().FirstOrDefaultAsync();
            if (settings == null)
            {
                settings = new SampleSettings { TimeInterval = newSettings.TimeInterval, MaxLoadTime = newSettings.MaxLoadTime };
                await _context.SampleSettings.AddAsync(settings);
                await _context.SaveChangesAsync();
                return settings;
            }
            settings.TimeInterval = newSettings.TimeInterval;
            settings.MaxLoadTime = newSettings.MaxLoadTime;
            settings.StartPage = newSettings.StartPage;
            settings.Email = newSettings.Email;
            _context.SampleSettings.Update(settings);
            await _context.SaveChangesAsync();

            return settings;
        }
    }
}
